import numpy
import cv2
import cam
import time

cv2.namedWindow("A",1)
with cam.CameraReader() as reader:
    while True:
        raw_img = reader.next()
        print len(raw_img)
        y=cv2.imdecode(numpy.frombuffer(raw_img,'u1'),1)
        cv2.imshow("A",y)
        x = cv2.waitKey(40) 
        if x == 27: break
        