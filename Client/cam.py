import httplib
import base64
import re
import threading
from Queue import LifoQueue
import Queue

content_len_re = re.compile("Content-Length: (\d+)")

host ="10.40.38.11"
url = "/mjpg/video.mjpg"
user = "root"
password = "pass"

class CameraReaderThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        h = httplib.HTTPConnection(host)
        h.request("GET", url, headers=
	    {"User-Agent": "HTTPStreamClient",
	    "Connection": "Keep-Alive",
	    "Cache-Control": "no-cache",
	    "Authorization": "Basic %s" % 
		    (base64.encodestring("%s:%s" %(user,password)),)
        })
        self.response = h.getresponse()
        self.images = LifoQueue()
        self.messages = LifoQueue()
    def next_image(self):
        bound = ""
        while not bound.endswith("\r\n\r\n"):
            bound += self.response.read(1)
        m = content_len_re.search(bound)
        clen = int(m.groups(1)[0])
        img_data = self.response.read(clen)
        # discard trailing '\r\n'
        self.response.read(2) 
        return img_data
    def run(self):
        done = False
        while not done:
            # try to clear out queue
            if not self.images.empty():
                try:
                    self.images.get_nowait()
                except Queue.Empty:
                    pass
            img = self.next_image()
            self.images.put(img)
            try:
                msg = self.messages.get_nowait()
                if msg == "stop":
                    done = True
            except Queue.Empty:
                pass
        self.response.close()

def _camera_connection():
    h = httplib.HTTPConnection(host)
    h.request("GET", url, headers=
	    {"User-Agent": "HTTPStreamClient",
	    "Connection": "Keep-Alive",
	    "Cache-Control": "no-cache",
	    "Authorization": "Basic %s" % 
		    (base64.encodestring("%s:%s" %(user,password)),)
    })
    response = h.getresponse()
    while True:
        bound = response.read(65)
        m = content_len_re.search(bound)
        clen = int(m.groups(1)[0])
        img_data = response.read(clen)
        # discard trailing '\r\n'
        response.read(2) 
        yield img_data

class CameraReader:
    def __init__(self):
        self.thread = CameraReaderThread()
        self.thread.start()
    def __enter__(self):
        return self
    def __exit__(self, type, value, tb):
        self.close()
    def next(self):
        return self.thread.images.get()
    def close(self):
        self.thread.messages.put("stop")

if __name__ == "__main__":
    with CameraReader() as r:
        with open("frame.jpg",'wb') as f:
            f.write(r.next())

