try:
    import wpilib
except ImportError:
    import fake_wpilib as wpilib
from datetime import datetime, timedelta
class LVMaxSensor:
    def __init__(self, analogchannel):
        self.ch = analogchannel
    def GetDistanceCM(self):
        return 1000. * self.ch.GetVoltage() / 4.9
class LimitSwitch:
    def __init__(self, analogchannel):
        self.ch = analogchannel
    def get (self):
        V = self.ch.GetValue()
        if V > 500:
            return True
        else: return False
class LimitSwitchStateMachine:
    def __init__(self, limit_fn1, button_fn1, limit_fn2, button_fn2):
        self.lm1 = limit_fn1
        self.button1 = button_fn1
        self.lm2 = limit_fn2
        self.button2 = button_fn2
        self.state = 0
    def Transition(self):
        lm1 = self.lm1()
        lm2 = self.lm2()
        #if lm2: print ('top');
        #if lm1: print ('bot');
        bt1 = self.button1()
        bt2 = self.button2()
        if lm1 and self.state in [1,2]:
            self.state = 3
        elif lm2 and self.state in [4,5]:
            self.state = 6
        elif bt1 and self.state == 0:
            self.state = 1
        elif bt1 and self.state == 2:
            self.state = 3
        elif bt2 and self.state == 2:
            self.state = 6
        elif not bt1 and self.state == 3:
            self.state = 0
        elif not bt1 and self.state == 1:
            self.state = 2
        elif bt2 and self.state == 0:
            self.state = 4
        elif bt2 and self.state == 5:
            self.state = 6
        elif not bt2 and self.state == 4:
            self.state = 5
        elif not bt2 and self.state == 6:
            self.state = 0
    def GetState(self):
        if self.state in [1,2]:
            # ramp go up!
            return 1
        if self.state in [4,5]:
            # ramp go down!
            return -.2
        # ramp hold position!
        return 0
jaguar1=wpilib.Jaguar(7)
jaguar2=wpilib.Jaguar(8)
jaguar3=wpilib.Jaguar(9)
jaguar4=wpilib.Jaguar(10)
rampguar = wpilib.Jaguar(4)

compressor=wpilib.Jaguar(5)
servo1 = wpilib.Servo(1,2)
solenoid1=wpilib.Solenoid(1)
solenoid2=wpilib.Solenoid(2) 
comp = wpilib.PWM(1)
pressureSwitch = wpilib.DigitalInput(14)
#rangefinder = LVMaxSensor(wpilib.AnalogChannel(1))
limitswitch = LimitSwitch (wpilib.AnalogChannel(1))
limitswitch2 = LimitSwitch (wpilib.AnalogChannel(2))
#compressor=wpilib.Compressor(1,5)
#compressor.Start()
import xbox
stick1 = wpilib.Joystick(1)
xbox1=xbox.XboxController(stick1)
digitalinput=wpilib.DigitalInput(3)
digitaloutput=wpilib.DigitalOutput(4) 
pwm3 = wpilib.PWM(3)
 
ramp = LimitSwitchStateMachine(limitswitch2.get, xbox1.A, limitswitch.get, xbox1.B) 

autonomous_begin_time = None
autonomous_begin_run, autonomous_end_run = None, None
autonomous_stuff=None
autonomous_more=None

def checkRestart():
    if xbox1.start():
        raise SystemExit("Restart")
    
def disabled():
    while wpilib.IsDisabled():
        checkRestart()
        compressor.Set(0)
        wpilib.Wait(0.01)
    
def moveforward (val):  
    global jaguar1, jaguar4
    jaguar1.Set(val*-1)
    jaguar4.Set(val)  
def toiletseatup ():
        solenoid1.Set(1)
        solenoid2.Set(0)
def toiletseatdown ():
        solenoid1.Set(0)
        solenoid2.Set(1)
def strafe (val):
        global jaguar2, jaguar3
        jaguar2.Set(val*.75)
        jaguar3.Set(val*.75)
def turn (val2):
        global jaguar2, jaguar3
        jaguar2.Set(val2*-1)
        jaguar3.Set(val2)
# drive for X seconds to travel to low goal
# Low goal approx. 15 feet 4 inches from start (?)
def autonomous ():
    global autonomous_begin_time, autonomous_begin_run, autonomous_end_run, autonomous_stuff, autonomous_more
    #set to "no score" to disable attempting to score into low goal (insert no score instead of score when we need not to score)
    mode = "score"
    if not autonomous_begin_time:
        if mode == "score": 
            b = (0, 6, 7, 8)
        elif mode == "no score":
            b = (0, 5, 0, 0)
        autonomous_begin_time = datetime.now()
        autonomous_begin_run = autonomous_begin_time + timedelta(seconds=b[0])
        autonomous_end_run = autonomous_begin_time + timedelta(seconds=b[1])
        autonomous_stuff= autonomous_begin_time + timedelta(seconds=b[3])
        autonomous_more= autonomous_begin_time + timedelta(seconds=b[2])
        print("begin: %s" % autonomous_begin_run)
        print("end: %s" % autonomous_end_run)
    wpilib.GetWatchdog().SetEnabled(False)
    while wpilib.IsAutonomous() and wpilib.IsEnabled():
        checkRestart()
        
        n = datetime.now()
        val = 0
        if autonomous_begin_run <= n <= autonomous_end_run:
            val=-0.5
        moveforward(val);
        
        if n >= autonomous_end_run:
            rampguar.Set(-.5)
        
        if n >= autonomous_more:
            toiletseatup();
       
        if n >= autonomous_stuff:
            toiletseatdown();
        
        wpilib.Wait(0.01)
        

        
def teleop():
    dog = wpilib.GetWatchdog()
    dog.SetEnabled(True)
    dog.SetExpiration(0.25)
    
    while wpilib.IsOperatorControl() and wpilib.IsEnabled():
        dog.Feed()
        ramp.Transition()
        state = ramp.GetState() 
        rampguar.Set(state)
        psv = pressureSwitch.Get();
        if psv == 1:
            compressor.Set(0)
        elif psv == 0:
            compressor.Set(1) 
        else:
            print ("badurk")
        if xbox1.Y():
            toiletseatup();
        elif xbox1.X():
            toiletseatdown();  
        if xbox1.A():
            rampguar.Set(1)
        elif xbox1.B():
            rampguar.Set(-.5)  
        val=xbox1.left_joystick_axis_v()
        moveforward(val);
        val=xbox1.left_joystick_axis_h()
        val2=xbox1.right_joystick_axis_h()
        if abs (val) > abs (val2):
            strafe(val);
        else:
            turn(val2);
        #x=xbox1.right_trigger()
        #jaguar4.Set(x)
        #print ("right trigger:",x)   
        #print ("get range:", range)
        raw = pwm3.GetRaw()
        #print ("pwm: ", raw)
        checkRestart()
        wpilib.Wait(0.01)

def run():
    """Main loop"""
    while 1:
        if wpilib.IsDisabled():
            print("Running disabled()")
            disabled()
            while wpilib.IsDisabled():
                wpilib.Wait(0.01)
        elif wpilib.IsAutonomous():
            print("Running autonomous()")
            autonomous()
            while wpilib.IsAutonomous() and wpilib.IsEnabled():
                wpilib.Wait(0.01)
        else:
            print("Running teleop()")
            
            teleop()
            while wpilib.IsOperatorControl() and wpilib.IsEnabled():
                wpilib.Wait(0.01)